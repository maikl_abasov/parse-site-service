<?php

$router->addRoute('GET', '/', 'Dzion\App\Controllers\HomeController::index');
$router->addRoute('GET', '/films/get-categories', 'Dzion\App\Controllers\HomeController::getFilmCategories');
$router->addRoute('GET', '/films/get-list/{category_id}', 'Dzion\App\Controllers\HomeController::getRatingFilmsList');
$router->addRoute('GET', '/films/parse-pages-start', 'Dzion\App\Controllers\HomeController::parsePagesStart');
$router->addRoute('GET', '/films/clear-film-info-table', 'Dzion\App\Controllers\HomeController::clearFilmInfoTable');
$router->addRoute('GET', '/films/get-date-menu', 'Dzion\App\Controllers\HomeController::getDateMenu');
$router->addRoute('GET', '/films/get-list-to-date/{category_id}/{select_date}', 'Dzion\App\Controllers\HomeController::getListToDate');

$router->addRoute('GET', '/films/parse-full-meter', 'Dzion\App\Controllers\HomeController::parseRatingFullMeterFilms');
$router->addRoute('GET', '/films/parse-west-serials', 'Dzion\App\Controllers\HomeController::parseRatingWestSerials');
$router->addRoute('GET', '/films/parse-japan-dorama', 'Dzion\App\Controllers\HomeController::parseRatingJapanesDorama');
$router->addRoute('GET', '/films/parse-korean-dorama', 'Dzion\App\Controllers\HomeController::parseRatingKoreanDorama');
$router->addRoute('GET', '/films/parse-russian-serials', 'Dzion\App\Controllers\HomeController::parseRatingRussianSerials');

// $router->addRoute('GET', '/about', 'Dzion\App\Controllers\HomeController::about');
// $router->addRoute('GET', '/{link}!{from}', 'App\HomeController::about');
