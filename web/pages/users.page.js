Vue.component('users-page', {
    data: function () {
        return {
            selectClass : 'mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md',
            loadStrategy   : 'all',
            usersLocalList : [],
            usersHistoryList : [],
            secTimer       : 3000,
            userTimer      : '',
            connectsDateList : [],

        }
    },

    created() {

        let strategy = this.getLocalStore('load-strategy');
        if(strategy)
            this.loadStrategy = strategy;

        this.selectLoadStrategy();
        this.getHistoryDateList();

    },

    mounted() {
        this.usersConnectsTimer();
    },

    methods : {

        onSelectedToDate(event) {
            let date = event.target.value;
            this.getUsersConnectsToDate(date);
        },

        async getUsersConnectsToDate(date) {
            let action = '?action=user-connects-to-date&select-date=' + date
            let response = await this._get(action);
            this.usersHistoryList = response;
        },

        async getHistoryDateList() {
            let response = await this._get('?action=connect-date-list');
            this.connectsDateList = response;
        },

        async getUsersConnectsLoad(action = 'user-connects') {
             let users = await this.gitUsersConnects(action);
             this.usersLocalList = this._copy(users);
        },

        usersConnectsTimer() {
            this.userTimer = setInterval(() => {
                this.selectLoadStrategy();
            }, this.secTimer)
        },

        setLoaderStrategy(strategy){
            this.loadStrategy = strategy;
            this.setLocalStore('load-strategy', strategy);
        },

        selectLoadStrategy() {
            let action = 'user-connects'
            switch (this.loadStrategy) {
                case 'all'   : break;
                case 'group' :
                    action = 'user-connects-group'
                    break;
            }
            this.getUsersConnectsLoad(action);
        },

    },

    template: `
    <div class="bids-page-container" >
    
         <main class="min-w-0 flex-1 border-t border-gray-200 lg:flex" style="border: 0px gainsboro solid">

            <!---- КОНТЕЙНЕР ДЛЯ ПОКАЗА ПОЛЬЗОВАТЕЛЕЙ ---> 
            <section style="border: 1px gainsboro solid; padding:3px; " 
                     class="min-w-0 flex-1 h-full flex flex-col overflow-hidden lg:order-last" >
                     <!--- Заголовок --->
                     <div><div class="pb-5 border-b border-gray-200 sm:pb-0" 
                               style="height: 58px; border:2px gainsboro dotted; margin: 0px 0px 10px 0px; display: flex">
                               
                               <div><button @click="setLoaderStrategy('all')" class="my-custom-btn-class" 
                                            :class="{ 'user-strategy-menu-active' : loadStrategy == 'all' }" 
                                    >Все записи</button></div>
                               
                               <div><button @click="setLoaderStrategy('group')" class="my-custom-btn-class"
                                            :class="{ 'user-strategy-menu-active' : loadStrategy == 'group' }" 
                                    >Сгруппировать записи</button></div>     
                     </div></div>
  
                     <!--- Контент --->
                     <div class="page-content-container" >
                          <users-table :users_list="usersLocalList" ></users-table>
                     </div>
                     <!--- ./ Контент --->
            </section>
           <!---- КОНТЕЙНЕР ДЛЯ ПОКАЗА ПОЛЬЗОВАТЕЛЕЙ ---> 

           <!---- КОНТЕЙНЕР ДЛЯ ПОКАЗА ПОЛЬЗОВАТЕЛЕЙ ПО ДАТАМ---> 
           <section  style="border: 1px gainsboro solid; padding:3px;"
                     class="min-w-0 flex-1 h-full flex flex-col overflow-hidden lg:order-last">  
                     
                     <!--- Заголовок --->
                     <div><div class="pb-5 border-b border-gray-200 sm:pb-0" 
                               style="height: 58px; border:2px gainsboro dotted; margin: 0px 0px 10px 0px; display: flex">
                               
                               <div style="width: 200px; border:1px gainsboro solid; border-radius: 2px; padding:2px; margin:2px;">
                                   <div class="block text-sm font-medium text-gray-700" style="font-size: 10px; font-style: italic">Выбор по дате</div>
                                   <select @change="onSelectedToDate($event)" id="select-to-date" :class="selectClass" 
                                           style="cursor: pointer; width: 100%; border:1px gainsboro solid; border-radius: 0px; padding:0px" >
                                           <option v-for="value in connectsDateList" :value="value">
                                               {{ value }}</option>
                                   </select>
                               </div>
                               
                               <div>
                                   
                               </div>     
                     </div></div>

                     <!--- Контент --->
                     <div class="page-content-container" >
                           <users-table :users_list="usersHistoryList" ></users-table>
                     </div>
                     <!--- ./ Контент --->
                     
            </section>
            <!---- КОНТЕЙНЕР ДЛЯ ПОКАЗА ПОЛЬЗОВАТЕЛЕЙ ПО ДАТАМ---> 
 
          </main>
    </div>
    `,
})

