Vue.component('parser-page', {
    data: function () {
        return {
            loadStrategy   : 1,
            parserList     : [],
            filmCategories : [],
            fullParserBtn  : true,
        }
    },

    created() {
        // this.getFilmCategories();
        // this.getDateMenu();
        this.getParserList(this.loadStrategy);
    },

    mounted() {
        this.getEvent('set-load-stategy', (resp) => {
            this.setLoaderStrategy(resp.strategy)
        });

        this.getEvent('load-to-select-date', (resp) => {
            let catId = resp.strategy;
            let date  = resp.date;
            this.loadStrategy = catId;
            this.getParserListToDate(catId, date)
        });
    },

    methods : {

        async getParserList(categoryId) {
            let action = '/films/get-list/' + categoryId;
            let response = await this._get(action);
            this.parserList = response;
        },

        async getParserListToDate(categoryId, date) {
            let action = '/films/get-list-to-date/' + categoryId + '/' + date;
            let response = await this._get(action);
            this.parserList = response;
        },

        setLoaderStrategy(type) {
            this.loadStrategy = type;
            this.getParserList(this.loadStrategy);
        },

        async parsePagesStart() {
            let action = '/films/parse-pages-start';
            alert('Парсинг фильмов запущен, это займет некоторое время');
            let response = await this._get(action);
            this.getParserList(this.loadStrategy);
            alert('Парсинг фильмов успешно выполнен');
        },

        async clearFimlInfoTable() {
            let action = '/films/clear-film-info-table';
            let response = await this._get(action);
            this.getParserList(this.loadStrategy);
        },

    },

    template: `
    <div class="bids-page-container" >
    
         <main class="min-w-0 flex-1 border-t border-gray-200 lg:flex" style="border: 0px gainsboro solid">

            <!---- КОНТЕЙНЕР ДЛЯ ПОКАЗА СПИСКА ФИЛЬМОВ ---> 
            <section style="border: 1px gainsboro solid; padding:3px; " 
                     class="min-w-0 flex-1 h-full flex flex-col overflow-hidden lg:order-last" >
                     <!--- Заголовок --->
                     <div><div class="pb-5 border-b border-gray-200 sm:pb-0" 
                               style="height: 140px; border:2px gainsboro dotted; margin: 0px 0px 10px 0px; display: flex">                   
                               <films-menu :strategy="loadStrategy" _state="1" ></films-menu>
                               <films-menu :strategy="loadStrategy" _state="2" ></films-menu>
                               <films-menu :strategy="loadStrategy" _state="3" ></films-menu>
                               <films-menu :strategy="loadStrategy" _state="4" ></films-menu>
                               <films-menu :strategy="loadStrategy" _state="5" ></films-menu>                      
                     </div></div>
  
                     <!--- Контент --->
                     <div class="page-content-container" >
                          <parser-table :parser_list="parserList" ></parser-table>
                     </div>
                     <!--- ./ Контент --->
            </section>
           <!---- ./ КОНТЕЙНЕР ДЛЯ ПОКАЗА СПИСКА ФИЛЬМОВ ---> 

           <!---- КОНТЕЙНЕР ДЛЯ ПОКАЗА ФИЛЬМА---> 
           <section  style="border: 1px gainsboro solid; padding:3px;"
                     class="min-w-0 flex-1 h-full flex flex-col overflow-hidden lg:order-last">  
                     
                     <!--- Заголовок --->
                     <div><div class="pb-5 border-b border-gray-200 sm:pb-0" 
                               style="height: 58px; border:2px gainsboro dotted; margin: 0px 0px 10px 0px; display: flex">
 
                               <div><button @click="clearFimlInfoTable()" class="my-custom-btn-class" >Очистить таблицу фильмов</button></div>    
                               <div><button @click="parsePagesStart()" class="my-custom-btn-class" > Спарсить рейтинги фильмов </button></div>
                 
                     </div></div>

                     <!--- Контент --->
                     <div class="page-content-container" >
                           <film-container ></film-container>
                     </div>
                     <!--- ./ Контент --->
                     
            </section>
            <!---- ./ КОНТЕЙНЕР ДЛЯ ПОКАЗА ФИЛЬМА --> 
 
          </main>
    </div>
    `,
})

