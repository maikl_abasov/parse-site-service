
const HEADER_JWT_TOKEN_NAME = 'user-jwt-token'

const axiosHeaders = {
  'Accept' : 'application/json',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': '*',
  'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS',
  'X-Requested-With': 'XMLHttpRequest',
  'X-User-Jwt-Token': '',
  'user-jwt-token'  : '',
}

axios.defaults.headers = axiosHeaders

const ApiService = {
  install (Vue, options) {
    Vue.mixin({

      data: () => ({
         apiUrl: 'public/index.php',
      }),

      methods: {

        _post (url, data = null) {
              const apiUri = this.apiUrl + url
              const token = this.getToken()
              axios.defaults.headers[HEADER_JWT_TOKEN_NAME] = token
              return new Promise((resolve, reject) => {
                axios['post'](apiUri, data)
                    .then(response => {
                      let result = response.data
                      resolve(result)
                    }).catch(error => {
                      reject(error)
                    })
              })
        },

        _get (url) {

                const apiUri = this.apiUrl + url
                const token = this.getToken()
                axios.defaults.headers[HEADER_JWT_TOKEN_NAME] = token

                return new Promise((resolve, reject) => {
                    axios['get'](apiUri)
                      .then(response => {
                        let result = response.data;
                        resolve(result)
                      }).catch(error => {
                        reject(error)
                      })
                })
        },

        getToken () {
              let token = 'gfhfggfhfghfhggf'
              return token
        }

      }, //// --- Methods --
    }) //////// VueMixin
  }
}
