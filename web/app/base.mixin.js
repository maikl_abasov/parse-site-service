const BaseMixin = {

    install(Vue, options) {

        Vue.mixin({

            data: () => ({

                selectClass : 'mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md',
                inputClass  : 'appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm',
                btnClass    : 'w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500',

                userId   : 0,
                userRole : '',
                userName : '',

                users : [],
                profiles : [],

                userOnlineList : [],
                selectDateMenu : [],
                filmCategories : [],

            }),

            methods: {

                async getDateMenu() {
                    let action = '/films/get-date-menu';
                    let response = await this._get(action);
                    this.selectDateMenu = response;
                },

                async getFilmCategories() {
                    let action = '/films/get-categories';
                    let response = await this._get(action);
                    this.filmCategories = response;
                },

                getLocalStore(key) {
                    let value = localStorage.getItem(key);
                    return (value) ? value : false
                },

                setLocalStore(key, value) {
                    localStorage.setItem(key, value);
                },

                deleteLocalStore(key) {
                    localStorage.removeItem(key);
                },

                setUserInfo(user, token) {
                    this.setLocalStore('token', token);
                    this.setLocalStore('user_id', user.id);
                    this.setLocalStore('role', user.role);
                    this.setLocalStore('fio', user.fio);
                },

                deleteUserInfo() {
                    this.deleteLocalStore('token');
                    this.deleteLocalStore('user_id');
                    this.deleteLocalStore('role');
                    this.deleteLocalStore('fio');
                },

                redirectUrl(href) {
                    window.location.href = href;
                },

                getEvent(eventName, fn) {
                    EventBus.$on(eventName, fn)
                },

                sendEvent(eventName, data) {
                    EventBus.$emit(eventName, data);
                },

                _copy(items) {
                    return Object.assign({}, items);
                },

            } // Methods
        }) // VueMixin
    }
}

