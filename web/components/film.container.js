Vue.component('film-container', {

    data: function () {
        return {
            film : {},
        }
    },

    created() {
    },

    computed : {
    },

    mounted  () {

        this.getEvent('load-film-event', (film) => {
             this.film = film;
        });

    },

    methods : {
    },

    template: `
    <div class="lots-container" style="padding:0px" >
        <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg" 
                 style="border-radius: 0px; width: 100%">
     
                 <!--- ЗАГОЛОВОК ---> 
                 <h3 style="font-weight: bold; color:grey; font-size: 20px; color:green">{{film.name}}</h3>
                 <hr style="height: 5px; margin:5px;">

                 <div style="margin:5px;"><div class="grid" >
                         
                            <div class="" style="width:100%; border: 1px lightskyblue solid; padding:3px; border-radius: 2px; background: #9fcdff;
                                                 box-shadow: 0 8px 15px rgba(0,0,0,0.25), 0 6px 6px rgba(0,0,0,0.22);">
                               
                               <!----- Содержимое карточки --->
                               <div style="display: flex" >
                              
                                   <div class="flex-11 truncate1" style="width:50%" >
                                   
                                        <div class="flex items-center space-x-3">
                                          <h4 class="text-gray-900 text-sm font-medium truncate">Название: 
                                             <span class="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium bg-yellow-100 text-yellow-800" 
                                                   style="background: #28a745; color:white; ;">  {{film.name}} </span>
                                           </h4>  
                                        </div>
                                        
    <!--                                    <p class="mt-1 text-grey-500 text-sm truncate">Vin: </p>-->
                                        <p class="mt-1 text-grey-500 text-sm truncate"> Средний балл: {{film.average}}</p>
                                        <p class="mt-1 text-grey-500 text-sm truncate"> Дата загрузки: {{film.created_dt}} </p>
                                        <p class="mt-1 text-grey-500 text-sm truncate"> Краткое содержание : </p>
                                        <div class="mt-11 text-grey-500 text-sm truncate1" 
                                           style="text-align: justify; padding:12px; width:100%; font-style: italic" > {{film.short_desc}} </div>
                                        
                                   </div>
                                   <div style="width:50%">
                                       <img :src="'http://www.world-art.ru/cinema/' + film.img" alt="" style="width: 100%">
                                   </div>
                                   
                               </div>
                               <!----- ./ Содержимое карточки --->
                               
                               <!----- Ставка и минимальная цена --->

                               <div><hr>
                                   <div class="mt-px flex1 divide-x divide-gray-200" style="display: flex">
                                            <div class="w-0 flex-1 flex" >
                                                  <a href="#" style="width: 100%; border-radius: 0px;" >
                                                     <div class="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium bg-red-100 text-red-800" 
                                                          style="width: 100%; border-radius: 0px;"> Расчетный балл: {{film.point}} </div>
                                                  </a>
                                            </div>
                                            <div class="ml-px w-0 flex-1 flex" style="border: 0px;">
                                                  <a href="#" class="" style="width: 100%; border-radius: 2px;" >   
                                                     <div class="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium bg-green-100 text-green-800" 
                                                          style="width: 100%; border-radius: 0px;"> Голоса: {{film.vote}} </div>
                                                  </a>
                                           </div> 
                                   </div>
                               </div>
                               <!----- Ставка и минимальная цена --->
                          </div>
                    
                </div></div>
<!--            <pre>{{film}}</pre>-->
        
        </div>
    </div>`,
})
