Vue.component('parser-table', {
    props : ['parser_list'],
    data: function () {
        return {
            thStyle : 'px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider',
        }
    },

    computed : {
        parserList() {
           return  this.parser_list;
        },

        itemsCount() {
            return this.parser_list.length;
        },
    },

    methods : {
        loadFilm(film) {
             this.sendEvent('load-film-event', film);
        }
    },

    template: `
    <div class="users-table-container" style="padding:4px" ><div class="flex flex-col mt-2" >
        <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg" style="border-radius: 0px;" >
             <div style="font-size: 10px; font-style: italic" > Количество записей : {{this.itemsCount}}</div>    
             <table class="min-w-full divide-y divide-gray-200 parser-list-table" style="border-radius: 0px;">
             
                    <thead><tr>
                            <th class="" valign="top" > № </th>    
                            <th class="" valign="top" > Наименование </th>
                            <th class="" valign="top" > Расчетный балл </th>
                            <th class="" valign="top" > Голосов </th> 
                            <th class="" valign="top" > Средний балл </th>
                            <th class="" valign="top" > Дата загрузки </th>
                    </tr></thead>
                    
                    <tbody class="bg-white divide-y divide-gray-200" >
                        <template v-for="(item) in parserList"><tr :key="item.film_id" class="bg-white" @click="loadFilm(item)" style="cursor:pointer;">
                           
                             <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-500" style="width: 20px">
                                 <span class="text-gray-900 font-medium">  
                                  {{item.num}}</span>
                             </td>
                             
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-500" style="width: 150px; text-align: left">
                                 <span class="text-gray-900 font-medium">  
                                  {{item.name}}</span>
                            </td> 
                            
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-500" style="width: 40px">
                                 <span class="text-gray-900 font-medium">  
                                  {{item.point}}</span>
                            </td>
                             
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-500" style="width: 40px">
                                 <span class="text-gray-900 font-medium">  
                                  {{item.vote}}</span>
                            </td>
                            
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-500" style="width: 40px">
                                 <span class="text-gray-900 font-medium">  
                                  {{item.average}}</span>
                            </td>
                            
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-500" style="width: 120px">
                                 <span class="text-gray-900 font-medium">  
                                  {{item.created_dt}}</span>
                            </td>
               
                        </tr></template>
                    </tbody>
            </table>
                 
        </div>
    </div></div>
    `,
})
