Vue.component('header-page', {
    data: function () {
        return {}
    },
    template: `

        <!----- ВЕРХНЕЕ МЕНЮ ------->
        <!--    <header class="flex flex-shrink-0">-->
        <!--        <div class="flex-shrink-0 px-4 py-3 bg-gray-800 w-64">-->
        <!--            <div class="flex items-center block w-full" style=color:white;" >Admin page</div>-->
        <!--        </div>-->
        <!--        <div class="flex-1 flex bg-gray-700 px-6 items-center justify-between" style="height: 50px" >-->
        <!--            <nav style="cursor:pointer">-->
        <!--                <a @click="setPageName('bids-page')"  class="ml-2 hover:bg-gray-600  inline-block text-sm font-medium text-white px-3 py-2 leading-non" :class="{ 'current-page-menu-active' : pageName == 'bids-page' }" >Ставки</a>-->
        <!--                <a @click="setPageName('users-page')" class="ml-2 hover:bg-gray-600  inline-block text-sm font-medium text-white px-3 py-2 leading-non" :class="{ 'current-page-menu-active' : pageName == 'users-page' }">Пользователи</a>-->
        <!--            </nav>-->
        <!--        </div>-->
        <!--    </header>-->
        <!----- ./ ВЕРХНЕЕ МЕНЮ ------->

        <nav class="bg-gray-800" >

                 <div class="max-w-7xl mx-auto px-2 sm:px-4 lg:px-8">
                    <div class="relative flex items-center justify-between h-16">

                        <div class="flex items-center px-21 lg:px-0">
                            <div class="flex-shrink-0" style=" padding:2px">
<!--                                <img class="lg:block h-8 w-auto" src="/img/rrt-auction_logo.png" alt="RRT-Auction" style="width: 120px; border-radius: 4px;">-->
                                <img class="h-10" src="https://tailwindui.com/img/logos/workflow-mark.svg?color=warmGray&amp;shade=400" alt="Company name">
                            </div>
                            <div class="lg:block lg:ml-8">
                                <div class="flex space-x-4">
                                   <!--- ВЕРХНЕЕ МЕНЮ   --->   
                                   <slot name="nav-menu"></slot>
                                   <!--- ./ ВЕРХНЕЕ МЕНЮ --->   
                                </div>
                            </div>
                        </div>

                        <div class="flex-1 flex justify-right px-2 lg:ml-2 lg:justify-end">
                            <div class="max-w-lg w-full lg:max-w-xs">
                                <label for="search" class="sr-only"> Поиск </label>
                                <div class="relative">
                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                        <svg class="h-5 w-5 text-gray-400" x-description="Heroicon name: solid/search"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                                        </svg>
                                    </div>
                                    <input id="search" name="search"
                                           class="block w-full pl-10 pr-3 py-2 border border-transparent rounded-md leading-5 bg-gray-700 text-gray-300 placeholder-gray-400 focus:outline-none focus:bg-white focus:border-white focus:ring-white focus:text-gray-900 sm:text-sm"
                                           placeholder="Search" type="search" style="border-radius: 0px">
                                </div>
                            </div>
                        </div>

                        <div class="hidden lg:block lg:ml-4">
                            <div class="flex items-center">
                                <button class="flex-shrink-0 bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                                    <span class="sr-only">View notifications</span>
                                    <svg class="h-6 w-6" x-description="Heroicon name: outline/bell"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>


        <!--                <div  class="ml-4 relative flex-shrink-0">-->
        <!--                    <div>-->
        <!--                        <button type="button" class="bg-gray-800 rounded-full flex text-sm text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" id="user-menu" aria-expanded="false" @click="open = !open" aria-haspopup="true" x-bind:aria-expanded="open">-->
        <!--                            <span class="sr-only">Open user menu</span>-->
        <!--                            <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2&amp;w=256&amp;h=256&amp;q=80" alt="">-->
        <!--                        </button>-->
        <!--                    </div>-->
        <!---->
        <!--                    <transition enter-active-class="transition ease-out duration-100" enter-class="transform opacity-0 scale-95" enter-to-class="transform opacity-100 scale-100" leave-active-class="transition ease-in duration-75" leave-class="transform opacity-100 scale-100" leave-to-class="transform opacity-0 scale-95">-->
        <!--                        <div x-description="Dropdown menu, show/hide based on menu state." x-show="open" class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu">-->
        <!--                            <a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">Your Profile</a>-->
        <!--                            <a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">Settings</a>-->
        <!--                            <a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">Sign out</a>-->
        <!--                        </div>-->
        <!--                    </transition>-->
        <!---->
        <!--                </div>-->

                    </div>
                </div>
        </nav>
    `,
})


