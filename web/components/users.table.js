Vue.component('users-table', {
    props : ['users_list'],
    data: function () {
        return {
            thStyle : 'px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider',
        }
    },

    computed : {
        getItemsList() {
           return  this.users_list;
        },
    },

    template: `
    <div class="users-table-container" style="padding:4px" ><div class="flex flex-col mt-2" >
        <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg" style="border-radius: 0px;" >
                
             <table class="min-w-full divide-y divide-gray-200" style="border-radius: 0px;">
                    <thead><tr>
                            <th class="" valign="top">
                              Дата
                            </th>    
                            <th class="" valign="top" >
                              Id профиля
                            </th>
                            <th class="" valign="top">
                               Имя профиля
                            </th>
                            <th class="" valign="top">
                               Имя пользователя
                            </th> 
                            <th class="" valign="top">
                               Url
                            </th>
                    </tr></thead>
                    
                    <tbody class="bg-white divide-y divide-gray-200" >
                        <template v-for="(item) in getItemsList"><tr :key="item.id" class="bg-white" >
                           
                             <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-500" style="width: 120px">
                                 <span class="text-gray-900 font-medium">  
                                  {{item.dt}}</span>
                              </td>
                              <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                <a :href="'/admin/accounts/get_profile/?id=' + item.prId" 
                                    style="color:dodgerblue" target="_blank" > {{item.prId}} </a>   
                              </td>
                              <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                <span class="text-gray-900 font-medium">  
                                    {{item.cmpName}} </span>
                              </td>
                              <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                <span class="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium bg-green-100 text-green-800">
                                    {{item.user_name}} </span>    
                              </td>
                              <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                   <span class="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium bg-red-100 text-red-800">
                                     {{item.url}}  </span>
                              </td>
                              
                        </tr></template>
                    </tbody>
            </table>
                 
        </div>
    </div></div>
    `,
})
