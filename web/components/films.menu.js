Vue.component('films-menu', {
    props : ['strategy', '_state'],
    data: function () {
        return {
            loadStrategy : 0,
            selectedDate : '',
            title    : 'Полнометражные фильмы',
            parseUrl : '',
        }
    },

    computed: {

        getTitle() {
            let state = parseInt(this._state);
            switch (state) {
                case 1 : this.title = 'Полнометражные фильмы';
                         this.parseUrl = '/films/parse-full-meter';
                         break;

                case 2 : this.title = 'Западные сериалы';
                         this.parseUrl = '/films/parse-west-serials';
                         break;

                case 3 : this.title = 'Японские дорамы';
                         this.parseUrl = '/films/parse-japan-dorama';
                         break;

                case 4 : this.title = 'Корейские дорамы';
                         this.parseUrl = '/films/parse-korean-dorama';
                         break;

                case 5 : this.title = 'Русские сериалы';
                         this.parseUrl = '/films/parse-russian-serials';
                         break;
            }
            return this.title;
        },

        getStrategy() {
            return this.strategy
        },

        setState() {
            this.loadStrategy = this._state;
            return this.loadStrategy
        }

    },

    created() {
        this.getFilmCategories();
        this.getDateMenu();
    },

    methods : {
        setLoaderStrategy() {
            this.sendEvent('set-load-stategy', { 'strategy' : this.loadStrategy});
        },

        onSelectedToDate() {
            let date = { 'strategy' : this.loadStrategy, 'date' : this.selectedDate }
            this.sendEvent('load-to-select-date', date);
        },

        async parserStart() {
            let response = await this._get(this.parseUrl);
            this.getDateMenu();
            alert('Новые данные успешно загружены');
        },
    },

    template: `
        <div>
            <button @click="setLoaderStrategy(setState)" class="my-custom-btn-class" style="height: 43px; width: 99%"
                    :class="{ 'user-strategy-menu-active' : setState == getStrategy }" 
            >{{getTitle}}</button>
            
            <div style="width: 100%; border:1px gainsboro solid; border-radius: 2px; padding:2px; margin:2px;" >
                   <div class="block text-sm font-medium text-gray-700" 
                   style="font-size: 10px; font-style: italic">Выбор по дате</div> 
                   <select @change="onSelectedToDate()" v-model="selectedDate" :class="selectClass" 
                           style="cursor: pointer; width: 100%; border:1px gainsboro solid; border-radius: 0px; padding:0px" >
                           <option v-for="value in selectDateMenu[setState]" 
                                  :value="value" > {{ value }}</option>
                   </select>
            </div>   
            
            <hr>
            
            <div><button @click="parserStart()" class="my-custom-btn-class" style="width: 99%">Запустить парсер</button></div> 
                
       </div>
    `,
})

