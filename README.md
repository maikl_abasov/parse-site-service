## Сервис для парсинга сайта 
Тестовый проект

установка: composer install

в папке configs/  изменить файл default_.env  на .env
##### Использованы на серверной части:

    composer, компоненты Symfony (Response, Request, Dotenv и т.д),
    phpQuery, autoloader, league/Route, Mysql, php.7

######
Входной файл для серверной части : public/index.php
######
Вся логика серверной части находится в папке : src
######
Файл маршрутизации находится : configs/routes.php
######
Серверная часть работает в режиме: REST API


##### Использованы на клиентской части:

    Vue.js, Axios, tailwindcss

#####
Входной файл для серверной части : index.php
######
Вся логика клиентской части находится в папке : web
######
Клиенская часть работает без перезагрузки страницы

###
##### Маршрутизация:

######
'GET', '/'
######
'GET', '/films/get-categories'
######
'GET', '/films/get-list/{category_id}'
######
'GET', '/films/parse-pages-start'
######
'GET', '/films/clear-film-info-table'
######
'GET', '/films/get-date-menu'
######
'GET', '/films/get-list-to-date/{category_id}/{select_date}'
######

'GET', '/films/parse-full-meter' 
######
'GET', '/films/parse-west-serials'
######
'GET', '/films/parse-japan-dorama'
######
'GET', '/films/parse-korean-dorama'
######
'GET', '/films/parse-russian-serials'
######




