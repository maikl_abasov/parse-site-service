<?php

set_time_limit(0);

define('ROOT_DIR'   ,  __DIR__ );
define('UPLOAD_DIR' ,  ROOT_DIR . '/../uploads/');
define('CONFIG_DIR' ,  ROOT_DIR . '/../configs/');
define('LOG_DIR'    ,  ROOT_DIR . '/../logs/');
define('SRC_DIR'    ,  ROOT_DIR . '/../src/');
define('LIB_DIR'    ,  ROOT_DIR . '/../lib/');

require '../vendor/autoload.php';

$router  = new Dzion\Engine\Router();
require_once CONFIG_DIR . 'routes.php';
require_once LIB_DIR . 'phpQuery.php';

$app = new Dzion\Engine\AppKernel($router, []);
$app->init();

