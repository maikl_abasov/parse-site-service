<?php

namespace Dzion\App\controllers;

use Symfony\Component\HttpFoundation\Response;
use Dzion\Engine\AbstractController;

class BaseController extends AbstractController
{
    protected $model;

    public function __construct()
    {
        $this->response = new Response();
    }

    protected function jsonRespond($data)
    {
        return $this->response->setContent(json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    }
}