<?php

namespace Dzion\App\Controllers;

use Dzion\App\Models\PageParse;
use Dzion\App\Services\PageParseService;
use Dzion\App\models\Home;

class HomeController extends BaseController
{
    protected $parseService;
    protected $parseModel;

    public function __construct()
    {
        parent::__construct();
        $this->model        = new Home();
        $this->parseService = new PageParseService();
        $this->parseModel   = new PageParse();
    }

    public function index()
    {
        $result = ['Home' => 'index'];
        return $this->jsonRespond($result);
    }

    public function getRatingFilmsList($request, $response, $args) {
        $categoryId = $args['category_id'];
        $list = $this->parseModel->getRatingFilmsList($categoryId);
        return $this->jsonRespond($list);
    }

    public function getListToDate($request, $response, $args) {
        $categoryId = $args['category_id'];
        $selectDate = urldecode($args['select_date']);
        $list = $this->parseModel->getListToDate($categoryId, $selectDate);
        return $this->jsonRespond($list);
    }

    public function getFilmCategories() {
        $categories = $this->parseModel->getFilmCategories();
        return $this->jsonRespond($categories);
    }

    public function getDateMenu() {
        $dateMenu = $this->parseModel->getDateMenu();
        return $this->jsonRespond($dateMenu);
    }

    public function clearFilmInfoTable() {
        $this->parseModel->clearFilmInfoTable();
    }

    public function parsePagesStart(){

        $this->parseRatingFullMeterFilms();
        $this->parseRatingWestSerials();
        $this->parseRatingJapanesDorama();
        $this->parseRatingKoreanDorama();
        $this->parseRatingRussianSerials();

    }

    // ---- Полнометражные фильмы ----
    public function parseRatingFullMeterFilms() {
        $categoryId = 1;
        $pageUrl    = 'rating_top.php';
        $parseResult  = $this->parseService->ratingPageParse($pageUrl, $categoryId);
        $response = $this->parseModel->saveParseResult($parseResult, $categoryId);
        return $response;
    }

    // Западные сериалы
    public function parseRatingWestSerials() {
        $categoryId = 2;
        return $this->parseRatingTvMeterFilms($categoryId, 1);
    }

    // Японские дорамы
    public function parseRatingJapanesDorama() {
        $categoryId = 3;
        return $this->parseRatingTvMeterFilms($categoryId, 2);
    }

    // Корейские дорамы
    public function parseRatingKoreanDorama() {
        $categoryId = 4;
        return $this->parseRatingTvMeterFilms($categoryId, 4);
    }

    // Российские сериалы
    public function parseRatingRussianSerials() {
        $categoryId = 5;
        return $this->parseRatingTvMeterFilms($categoryId, 3);
    }

    // --- Сериалы и дорамы ---
    protected function parseRatingTvMeterFilms($categoryId, $anchorNum = 1) {
        $pageUrl     = 'rating_tv_top.php?public_list_anchor=' . $anchorNum;
        $parseResult = $this->parseService->ratingTvPageParse($pageUrl, $categoryId);
        $response    = $this->parseModel->saveParseResult($parseResult, $categoryId);
        return $response;
    }

}