<?php

namespace Dzion\App\Services;

class PageParseService
{
    protected $topCount = 10;
    protected $filmsCount = 0;
    protected $step = 0;
    protected $resourceUrl;
    protected $parseResult = [];
    protected $buffer = [];

    public function __construct($resourceUrl = 'http://www.world-art.ru/cinema') {
        $this->resourceUrl = $resourceUrl;
    }

    // Рейтинги полномтражных фильфов
    public function ratingPageParse($resource = 'rating_top.php', $categoryId = 1)
    {
        $fileName  = 'word-art.txt';
        $dom = $this->getDomLoader($resource, $fileName);
        $result = $buffer = [];
        $step = $filmsCount = 0;

        foreach($dom->find('tr .review') as  $item) {

            $step++;
            $elem = $item->nodeValue;

            switch ($step) {
                case 1 : $buffer['num']     = $elem; break;
                case 2 : $buffer['name']    = $elem;
                         $el = pq($item);
                         $href = $el->attr("href");
                         $buffer['href'] = $href;

                         $filmInfo = $this->filmPageParse($href, $categoryId);
                         foreach ($filmInfo as $field => $info) {
                            $buffer[$field] = $info;
                         }

                         break;

                case 3 : $buffer['point']   = $elem; break;
                case 4 : $buffer['vote']    = $elem; break;
                case 5 : $buffer['average'] = $elem;
                         $result[] = $buffer;
                         $buffer   = [];
                         $step     = 0;
                         $filmsCount++;
                         break;
            }

            if($filmsCount > $this->topCount)
                break;
        }

        // lg($result);

        return $result;
    }

    // Рейтинги сериалов, дорам, корейских и т.д.
    public function ratingTvPageParse($resource, $categoryId)
    {
        $fileName  = 'word-art.txt';
        $dom = $this->getDomLoader($resource, $fileName);

        $this->parseResult = $this->buffer = [];
        $this->step = $this->filmsCount = $ch = 0;

        foreach($dom->find('tr .review') as  $item) {
            $ch++;
            if($ch == 1) continue;

            $this->step++;
            $this->setNodeValue($item, $categoryId);
            if($this->filmsCount > $this->topCount)
                break;
        }

        // lg($this->parseResult);

        return $this->parseResult;
    }

    public function filmPageParse($resource, $categoryId = 1)
    {
        $fileName  = 'item-info.txt';
        $dom = $this->getDomLoader($resource, $fileName);
        $result = [];
        $step   = 0;

        foreach($dom->find('.comment_block img') as  $item) {
            $el = pq($item);
            $src = $el->attr("src");
            break;
        }

        switch ($categoryId) {
            case 1 :
                $result = $this->fullMeterFilm($dom);  break;
            case 2 :
                $result = $this->westSerial($dom);    break;
            case 3 :
                $result = $this->japanDorama($dom);   break;
            case 4 :
                $result = $this->koreanDorama($dom);  break;
            case 5 :
                $result = $this->russianSerial($dom); break;
        }

        $result['img'] = $src;

        return $result;
    }

    protected function setNodeValue($item, $categoryId) {

        $elem = $item->nodeValue;

        switch ($this->step) {
            case 1 : $this->buffer['num']     = $elem; break;
            case 2 : $this->buffer['name']    = $elem;

                     $el = pq($item);
                     $href = $el->attr("href");
                     $this->buffer['href'] = $href;
                     $filmInfo = $this->filmPageParse($href, $categoryId);
                     foreach ($filmInfo as $field => $info) {
                         $this->buffer[$field] = $info;
                     }

                     break;

            case 3 : $this->buffer['point']   = $elem; break;
            case 4 : $this->buffer['vote']    = $elem; break;
            case 5 :  break;
            case 6 : $this->buffer['average'] = $elem;
                     $this->parseResult[] = $this->buffer;
                     $this->buffer   = [];
                     $this->step     = 0;
                     $this->filmsCount++;
                     break;
        }

    }

    protected function getDomLoader($resource, $fileName = 'word-art.txt') {
        $url = $this->resourceUrl . '/' . $resource;
        $html = $this->loadSitePage($url, $fileName);
        // $html = file_get_contents($url);
        $dom = \phpQuery::newDocument($html);
        return $dom;
    }

    protected function loadSitePage($url, $fileName = 'word-art.txt') {

        $filePath  = LOG_DIR  . '/parse/' . $fileName;
        $fp = fopen ($filePath, "w");

        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_USERAGENT, "google");
        curl_setopt ($ch, CURLOPT_FILE, $fp);
        curl_setopt ($ch, CURLOPT_HEADER, false);
        curl_exec ($ch);
        curl_close ($ch);
        fclose ($fp);

        $html = file_get_contents($filePath);
        return $html;
    }

    protected function westSerial($dom) {

        $step   = 0;
        $result = $tmp = [];

        foreach($dom->find('table .review') as  $item) {
            $step++;

            $elem = $item->nodeValue;
            switch ($step) {
                case 2   : $result['title']         = $elem; break;
                case 4   : $result['country_made']  = $elem; break;
                case 7   : $result['format']        = $elem; break;
                case 10  : $result['chrono_meters'] = $elem; break;
                case 14  : $result['series_count'] = $elem; break;
                case 16  : $result['genre']        = $elem; break;

                case 21  : $result['year']         = $elem; break;
                case 23  : $result['tv_canal']     = $elem; break;
                case 25  : $result['producer']     = $elem; break;
                case 35  : $result['roles']        = $elem; break;
                case 53  : $result['short_desc']   = $elem; break;
                // default  : $result[] = ['step' => $step, 'value' => $elem];break;
            }

            // if($step > 53) break;
            $tmp[] = ['step' => $step, 'value' => $elem];
        }

        foreach($dom->find('p[align=justify].review') as  $item) {
            $elem = $item->nodeValue;
            $result['short_desc'] = $elem;
            break;
        }

        return $result;
    }

    protected function japanDorama($dom) {

        $result = [];

        $step   = 0;
        foreach($dom->find('p.review') as  $item) {
            $step++;
            $elem = $item->nodeValue;
            switch ($step) {
                case 1  : $result['short_desc'] = $elem; break;
            }
            if($step > 1) break;
        }


        $step = 0;
        foreach($dom->find('tr .review') as  $item) {
            $step++;
            $elem = $item->nodeValue;
            switch ($step) {
                case 2   : $result['title']         = $elem; break;
                case 6   : $result['country_made']  = $elem; break;
                case 9   : $result['format']        = $elem; break;
                case 12  : $result['chrono_meters'] = $elem; break;
                case 16  : $result['series_count'] = $elem; break;
                case 18  : $result['genre']        = $elem; break;
                case 21  : $result['year']         = $elem; break;
                case 23  : $result['tv_canal']     = $elem; break;
                case 27  : $result['producer']     = $elem; break;
                case 45  : $result['roles']        = $elem; break;
                // case 53  : $result['short_desc']   = $elem; break;
                // default  : $result[] = ['step' => $step, 'value' => $elem];break;
            }

            if($step > 45) break;
        }

        return $result;
    }

    protected function koreanDorama($dom) {

        $result = [];

        $step = 0;
        foreach($dom->find('tr .review') as  $item) {
            $step++;
            $elem = $item->nodeValue;
            switch ($step) {
                 case 6   : $result['title']         = $elem; break;
                 case 8   : $result['country_made']  = $elem; break;
                 case 11  : $result['format']        = $elem; break;
                 case 14  : $result['chrono_meters'] = $elem; break;

                 case 18  : $result['series_count'] = $elem; break;
                 case 20  : $result['genre']        = $elem; break;
                 case 26  : $result['year']         = $elem; break;
                 case 28  : $result['tv_canal']     = $elem; break;
                 case 30  : $result['producer']     = $elem; break;
                 case 36  : $result['roles']        = $elem; break;
                 case 83  : $result['short_desc']   = $elem; break;
                 //default  : $result[] = ['step' => $step, 'value' => $elem];break;
            }

            if($step > 83) break;
        }

        foreach($dom->find('p[align=justify].review') as  $item) {
            $elem = $item->nodeValue;
            $result['short_desc'] = $elem;
            break;
        }

        return $result;
    }

    protected function russianSerial($dom) {
        $result = [];

        $step = 0;
        foreach($dom->find('tr .review') as  $item) {
            $step++;
            $elem = $item->nodeValue;
            switch ($step) {
                case 2   : $result['title']         = $elem; break;
                case 4   : $result['country_made']  = $elem; break;
                case 7   : $result['format']        = $elem; break;
                case 10  : $result['chrono_meters'] = $elem; break;
                case 14  : $result['series_count'] = $elem; break;
                case 16  : $result['genre']        = $elem; break;
                case 20  : $result['year']         = $elem; break;
                // case 23  : $result['tv_canal']     = ''; break;
                case 22  : $result['producer']     = $elem; break;
                case 33  : $result['roles']        = $elem; break;
                case 48  : $result['short_desc']   = $elem; break;
                // default  : $result[] = ['step' => $step, 'value' => $elem];break;
            }
            if($step > 48) break;
        }

        foreach($dom->find('p[align=justify].review') as  $item) {
            $elem = $item->nodeValue;
            $result['short_desc'] = $elem;
            break;
        }

        $result['tv_canal'] = '';

        return $result;
    }


    protected function fullMeterFilm($dom) {
        $result = [];

        $step = 0;
        foreach($dom->find('tr .review') as  $item) {
            $step++;
            $elem = $item->nodeValue;
            switch ($step) {
                case 2   : $result['title']         = $elem; break;
                case 4   : $result['country_made']  = $elem; break;
                case 8   : $result['format']        = $elem; break;
                case 10  : $result['chrono_meters'] = $elem; break;
                // case 12  : $result['series_count'] = $elem; break;
                case 12  : $result['genre']        = $elem; break;
                case 15  : $result['year']         = $elem; break;
                // case 23  : $result['tv_canal']     = ''; break;
                case 24  : $result['producer']     = $elem; break;
                case 36  : $result['roles']        = $elem; break;
                case 54  : $result['short_desc']   = $elem; break;
                // default  : $result[] = ['step' => $step, 'value' => $elem];break;
            }

            if($step > 54) break;
        }

        $result['tv_canal'] = '';
        $result['series_count'] = '';

        foreach($dom->find('p[align=justify].review') as  $item) {
            $elem = $item->nodeValue;
            $result['short_desc'] = $elem;
            break;
        }

        return $result;
    }

}