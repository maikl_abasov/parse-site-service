<?php

namespace Dzion\App\Models;

class PageParse extends BaseModel
{

    public function saveParseResult(array $data, $categoryId = 1) {
         $createdDate = date("Y-m-d H:i:s");
         foreach ($data as $key => $film) {
             $this->saveFilm($film, $categoryId, $createdDate);
         }
    }

    public function saveFilm(array $film, $categoryId, $createdDate) {

        $pdo  = $this->db;

        $insertQuery = "
            INSERT INTO `firm_info` (`num`, `name`, `point`, `vote`, `average`, `year`, `img`, `short_desc`, `created_dt`, `category_id`, `href`,
                                     `country_made`, `format`, `chrono_meters`, `genre`, `producer`, `roles`, `tv_canal`, `series_count`) 
                           VALUES   (:num,  :name,  :point,  :vote,  :average,  :year,  :img,  :short_desc,  :created_dt , :category_id, :href,
                                     :country_made, :format, :chrono_meters, :genre, :producer, :roles, :tv_canal, :series_count)
        ";

        $stmt = $pdo->prepare($insertQuery);

        $stmt->bindParam(':num'    , $film['num']);
        $stmt->bindParam(':name'   , $film['name']);
        $stmt->bindParam(':point'  , $film['point']);
        $stmt->bindParam(':vote'   , $film['vote']);
        $stmt->bindParam(':average', $film['average']);
        $stmt->bindParam(':year'   , $film['year']);
        $stmt->bindParam(':img'    , $film['img']);
        $stmt->bindParam(':short_desc' , $film['short_desc']);
        $stmt->bindParam(':created_dt' , $createdDate);
        $stmt->bindParam(':category_id', $categoryId);
        $stmt->bindParam(':href'   , $film['href']);

        $stmt->bindParam(':country_made'    , $film['country_made']);
        $stmt->bindParam(':format'   , $film['format']);
        $stmt->bindParam(':chrono_meters'  , $film['chrono_meters']);
        $stmt->bindParam(':genre'   , $film['genre']);
        $stmt->bindParam(':producer', $film['producer']);
        $stmt->bindParam(':roles'   , $film['roles']);
        $stmt->bindParam(':tv_canal'    , $film['tv_canal']);
        $stmt->bindParam(':series_count', $film['series_count']);

        $response =  $stmt->execute();
        return $response;
    }

    public function getFilmCategories() {
         $query = "SELECT * FROM `film_categories`";
         return $this->select($query);
    }

    public function getRatingFilmsList($categoryId) {
        $query = "SELECT * FROM `firm_info` WHERE `category_id` = {$categoryId}";
        return $this->select($query);
    }

    public function getListToDate($categoryId, $date) {
        $query = "SELECT * FROM `firm_info` WHERE `category_id` = {$categoryId} AND `created_dt` = '{$date}'";
        return $this->select($query);
    }

    public function clearFilmInfoTable() {
        $query = "TRUNCATE TABLE `firm_info`";
        return $this->select($query);
    }

    public function getDateMenu() {
        $query = "SELECT DISTINCT created_dt, category_id FROM `firm_info` ORDER BY film_id DESC";
        $response = $this->select($query);
        $result = [];
        foreach ($response as $key => $item) {
            $catId = $item['category_id'];
            $date = $item['created_dt'];
            $result[$catId][] = $date;
        }
        return $result;
    }

}