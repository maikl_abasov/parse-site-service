<?php

namespace Dzion\App\models;

use Symfony\Component\Dotenv\Dotenv;

use Dzion\Engine\Database;

class BaseModel
{
    protected $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function select($query) {
        $stm = $this->db->query($query);
        $results = $stm->fetchAll();
        return $results;
    }

    public function exec($query) {
        $pdo  = $this->db->getPdo();
        $updatedRows = $pdo->exec($query);
        return $updatedRows;
    }

    protected function errorInfo($stmt) {
        $errorInfo = $stmt->errorInfo();
        return $errorInfo;
    }

    protected function debugSql($stmt) {
        $result = $stmt->debugDumpParams();
        return $result;
    }

}