<?php

namespace Dzion\Engine;

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;
use League\Container\Container;
use Dzion\Engine\Database;
use Dzion\Engine\Router;

class AppKernel
{

    protected $dbConf;
    protected $router;

    public function __construct(Router $router, $dbConfig = []) {
        $this->dbConf = $dbConfig;
        $this->router = $router;
    }

    public function init() {

        //$database  = new Database($this->dbConf);
        //$container = new Container();

        $dotenv = new Dotenv();
        $dotenv->load(CONFIG_DIR . '.env');

        $dispatcher = $this->router->getDispatcher();
        $request    = Request::createFromGlobals();
        $response   = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
        $response->send();

    }

}