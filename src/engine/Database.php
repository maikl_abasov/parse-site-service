<?php

namespace Dzion\Engine;

class Database
{
    public static $pdo;
    // protected  $config;

    public function __construct() {
        //$this->config = $this->getConfig();
        //$this->connect();
    }

    protected static function connect($config) {

        $driver = (!empty($config['driver'])) ? $config['driver'] : 'mysql';
        $host   = $config['host'];
        $dbname = $config['dbname'];
        $user   = $config['user'];
        $password   = $config['password'];
        $dsn =  $driver . ':host=' .$host. ';dbname=' .$dbname. ';charset=utf8';

        $options = array(
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        );

        self::$pdo = new \PDO($dsn, $user, $password, $options);
    }

    protected static function getConfig() {
        return [
            'driver'   => $_ENV['DB_CONNECTION'],
            'host'     => $_ENV['DB_HOST'],
            'dbname'   => $_ENV['DB_DATABASE'],
            'user'     => $_ENV['DB_USERNAME'],
            'password' => $_ENV['DB_PASSWORD'],
        ];
    }

    public static function getInstance() {
        if(!empty(self::$pdo))
            return self::$pdo;
        $config = self::getConfig();
        self::connect($config);
        return self::$pdo;
    }

    //    public function getPdo() {
//        return self::$pdo;
//    }

}